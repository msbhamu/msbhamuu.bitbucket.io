$(function() {
 	
	/* Note: JS is used to set the space background height so that
	the background can work responsively without covering up the rest
	of the page. It needs to be fixed since they're animated divs and
	so to make sure it only covers a certain amount of space, this
	function is used.*/
	var setSpaceBgParams = function() {
		var parentHeight = $(".mu-hero-overlay").height();
		$(".stars, .twinkling, .clouds").height(parentHeight);

		var parentTop = $(".video-bg").parent().offset().top;
	
		$(".video-bg").css("top", parentTop);
	}

	setSpaceBgParams();

	$(window).resize(function() {
		setSpaceBgParams();
	})
});